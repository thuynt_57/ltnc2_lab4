/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex1 of lab4
 * Task		   : Xay dung mot so vi du ve cach su dung lop bao gom cac ham:
 				- Ham nhap du lieu vao
				- Ham xuat du lieu ra man hinh
				- Mot so ham so sanh tinh toan neu co
				- Ham constructor nhap truc tiep du lieu khi khai bao
				Cac lop vi du ve:
				- Lop Date ve ngay thang
				- Lop Bank account  
 * Reference   :
 * Date        : 24/4/2015
 */
 #include<iostream>
using namespace std;
class Date{
	private:
	    int d;
	    int m;
	    int y;
	public:
	    Date();
	    Date(int _d, int _m, int _y);
	    void setDate(int _d, int _m, int _y);
	    int getDate();
	    int getMonth();
	    int getYear();
	    bool compare(Date _date);
	    void print();
	    void input(){
	    	cout << "Enter the date: ";
	    	cin >> d;
	    	while ( d <= 0 || d >= 31 ){
	    		cout << "Retry: Enter the date ( 1 - 31 ): ";
	    		cin >> d;
	    	}
	    	
	    	cout << "Enter the month: ";
	    	cin >> m;
	    	while ( m <= 0 || m >=13 ){
	    		cout << "Retry: Enter the month ( 1 - 12 ): ";
	    		cin >> m;
	    	}
	    	
	    	cout << "Enter the year: ";
	    	cin >> y;
	    }
};

Date::Date(){
    d = m = y = 0;
}
Date::Date(int _d, int _m, int _y){
    d = _d;
    m = _m;
    y = _y;
}
void Date::setDate(int _d, int _m, int _y){
    d = _d;
    m = _m;
    y = _y;
}
int Date::getDate(){
    return d;
}
int Date::getMonth(){
    return m;
}
int Date::getYear(){
    return y;
}

bool Date::compare(Date _date){
    if ( d == _date.getDate() && m == _date.getMonth() && y == _date.getYear() ) {
        return true;
    }
    return false;
}
void Date::print(){
    cout << d << "-" << m << "-" << y << endl;
}
int main(){
    Date myBirthDay(15,11,1994);
    Date urBirthDay;
    cout << "Give me ur birth day" << endl;
    urBirthDay.input();
    cout << "My birth day is: ";
    myBirthDay.print();
	cout << "Your birth day is: ";
    urBirthDay.print();

    if( myBirthDay.compare(urBirthDay) ) {
        cout << "They are the same." << endl;
    }else{
        cout << "They are different. " << endl;
    }
    return 0;
}

