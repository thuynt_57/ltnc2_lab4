
/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex2 of lab4, multiplication is not implemented.
 * Task		   : Xay dung mot lop so lon bao gom cac chuc nang:
 				+ Ham constructor gan gia tri cho so do voi tham so la mot xau (C-String)
 				+ Ham hien thi gia tri cua so do ra man hinh
 				+ Cac toan tu cong, tru, nhan
 * Reference   : Tran Ngoc Linh'solution
 * Date        : 30/4/2015
 */
#include <iostream>
#include <string.h>

using namespace std;
class BigInteger{
	public:
	    BigInteger(string _value);
	    ~BigInteger();
	    int getSign();
	    int getLen();
	    string getValue();
	    void print();
	private:
	    string value;
	    int sign;
	    int len;
};
BigInteger::BigInteger(string _value){
    value = _value;
    
    // positive number
    if ( value[0] >= '0' &&value [0] <= '9') {
        sign=1;
    }
    while ( value[0] == '0') {
        value = value.substr(1, value.length() - 1);
    }
    sign = 1;
    len = value.length();
    
    //x negative number
    if ( value[0] == '-') {
        sign = -1;
        while ( value[1] == '0') {
            value = '-' + value.substr(2, value.length() - 1);
        }
        if ( value[1] == '0' ) {
            sign = 1;
            value = "0";
        }
        len = value.length();
    }
    
}
BigInteger::~BigInteger(){
    
}
int BigInteger::getSign(){
    return sign;
}
int BigInteger::getLen(){
    return len;
}
string BigInteger::getValue(){
    return value;
}
BigInteger operator - (BigInteger x1, BigInteger x2);

BigInteger operator + (BigInteger x1,BigInteger x2){
    string c;
    string a;
    string b;
    int sum = 0;
    int rem = 0;
    a = x1.getValue();
    b = x2.getValue();
    if ( x1.getSign() * x2.getSign() >0 ) {
        if ( x1.getSign() < 0) {
            a = a.substr(1,a.length() - 1);
            b = b.substr(1,b.length() - 1);
        }
        while ( a.length() > b.length() ) {
            b = '0' + b;
        }
        while ( a.length() < b.length() ){
            a = '0' + a;
        }
        for ( int i = a.length() - 1; i >= 0; i-- ) {
            sum = a[i] + b[i] - 2 * '0' + rem;
            rem = sum / 10;
            c = char(sum%10 + '0') + c;
        }
        if ( rem > 0 ) {
            c = char(rem + '0') + c;
        }
        if ( x1.getSign() < 0 ) {
            c = '-' + c;
        }
    }else{
        if ( x1.getSign() == 1 ) {
            b = x2.getValue();
            b = b.substr(1, b.length()-1);
            return BigInteger(x1.getValue()) - BigInteger(b);
        }
        if( x1.getSign() == -1){
            b = x1.getValue();
            b = b.substr(1,b.length()-1);
            return BigInteger(x2.getValue())-BigInteger(b);
            
        }
    }
    return BigInteger(c);
}

BigInteger operator - (BigInteger x1, BigInteger x2){
    string a;
    string b;
    string c;
    int rem = 0;
    int dau = 1;
    a = x1.getValue();
    b = x2.getValue();
    if( x1.getSign() * x2.getSign() > 0){
        if( x1.getSign() < 0 ){
            a = a.substr(1, a.length() - 1);
            b = b.substr(1, b.length() - 1);
        }
        while ( a.length() < b.length() ) {
            a = '0' + a;
        }
        while ( a.length() > b.length() ) {
            b = '0' + b;
        }
        for( int i = 0; i < a.length(); i++){
            if( a[i] < b[i]){
                string c1;
                c1 = a;
                a = b;
                b = c1;
                dau = x1.getSign()*-1;
                break;
            }else{
                break;
            }
        }
        for (int i = a.length() - 1; i >= 0; i--) {
            if( a[i] - b[i] - rem < 0 ){
                c = char(a[i] + 10 - b[i] - rem + '0') + c;
                rem = 1;
            }else{
                c = char(a[i] - b[i] - rem + '0') + c;
                rem = 0; 
            }
        }
        if ( dau*x1.getSign() == -1 && x1.getSign() == 1 ) {
                c = '-' + c;
        }
    }else{
        if ( x1.getSign() == 1)  {
            b = x2.getValue();
            b = b.substr(1, b.length());
            return BigInteger(x1.getValue()) + BigInteger(b);
        }
        if ( x1.getSign() == -1 ) {
            b = x2.getValue();
            b = '-' + b;
            return BigInteger(x1.getValue()) + BigInteger(b);
        }
    }
    return BigInteger(c);
}

void BigInteger::print(){
    cout << value;
}
int main(){
    BigInteger a("-1000001");
    BigInteger b("100006");

    a.print();
    cout << " + " ;
    b.print();
    cout << " = ";
    BigInteger c = a + b;
   c.print();
   cout << endl;
   
    a.print();
    cout << " - " ;
    b.print();
    cout << " = ";
   (a- b).print();
    return 0;
}
