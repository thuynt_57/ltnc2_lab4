/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex3 of lab4
 * Task		   : Xay dung mot lop sinh vien voi cac tinh nang co ban:
 				- Nhap du lieu vao
 				- Xuat du lieu ra man hinh
 				- Chuan hoa ten
 				- Tach tu, trar ve danh sach tu
 				- Gan xau
 				- So sach xau
 				- Ghep xau
 				- Dao nguoc xau
 				- Gan gia tri moi
 				Luu y: Du lieu luu duoi dang char*
 * Reference   :
 * Date        : 25/4/2015
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#define MAX 100
#define max 10

using namespace std;

// Global function for char*

void deleteLeftSpace( char *str ) {   
    int l = strlen(str);
    // If get the space in the beginning of the string, shift right
    while (str[0] == ' ') {
        for(int i = 0; i < l; i++)
            str[i] = str[i + 1];  
    }
}
 
void deleteRightSpace( char *str ) {
    int l = strlen(str);
    // If get the space in the end of the string, shift left
    while ( str[l-1] == 32 ) {
        str[l-1] = 0;
        l--;
    }
}
 
void standardize( char *str ) {
    int l = strlen(str);
    deleteLeftSpace(str);
    deleteRightSpace(str);
    // Delete spaces in the middle of the string
    for ( int i = 0; i < l; i++) {
        // If get 2 consecutive spaces, shift right 
        while( ( str[i] == 32 ) && ( str[i+1] == 32 )) {
            for ( int j = i; j < l; j++ ) {
                str[j] = str[j+1];
            }
        }
    }
}
 
// count the num of word
int count( char *str ) {   
	char *temp;  // We just want to count, not expect any change with our string, so we use temp var for this stuff 
	temp = str;
	standardize(temp);
    int l = strlen(temp);    
    int num = 0;
    // After standardize, if get empty string, return 0
    if (temp[0] == 0) {
        return num;
    }
    // Num of words = num of spaces + 1
    for ( int i = 0; i < l; i++ ) {
        if ( temp[i] == 32 ) {
            num++;
        }
    }
    return ( num + 1 );
}

// standardize name: all first letter of words are capital, others are normal
void standardizeName( char *str ){
	standardize(str);
	for ( int i = 0; i < strlen(str); i++ ) {
		if ( i == 0 || str[i-1] == 32 ) {
			str[i] = toupper(str[i]);
		}
		else {
			str[i] = tolower(str[i]);
		}
	}
}

// get a word, which begin at 'first' index and end at 'last' index, from a sentence
char* getWord( char* str, int first, int last ){	
    char* word = new char[max];
    for ( int i = first; i <= last; i++ ){
        word[i-first] = str[i];
    }
    word[last-first+1]='\0';
    return word;
}

// get all index of words from a sentence
int* getIndexOfWords( char *p ){
    int *indexs = new int[MAX];
	int t = 0;
    int first = 0;

    for ( int i = 0; i <= strlen(p); i++ ){
        if ( p[i] == ' ' || p[i] == '\0' ){
            indexs[t] = first; t++;
            indexs[t] = i; t++;
            first = i+1;
        }
    }
	return indexs;
}

///////////////////////////////////////////

class Student {
	private:
		char* name;
	public:
		Student(){
			name = new char[MAX];
		}		
		Student(string _data){
			name = new char[ _data.length() + 1 ];
			strcpy(name, _data.c_str());
		}
		Student(char* _data){
			name = new char[ strlen(_data) + 1 ];
			strcpy(name, _data);
		}
		~Student() {
			if(name) delete []name;
		}
//		- Nhap du lieu vao
		void input(){
			cout << "Enter student name: ";
			gets(name);
//			cin.ignore(1);
		}
//		- Xuat du lieu ra man hinh
		void print(){
			cout << "Student name: " << name << endl;
		}
//		- Chuan hoa ten
		void standardizeN();		
//		- Tach tu, trar ve danh sach tu
		void getWords();
//		- Gan xau
		void setName(char* _name){
			name = _name;
		}
//		- So sach xau
		void compareName(Student std);
//		- Ghep xau
		bool addStr(char* str);
//		- Dao nguoc xau
		void inverse();
};

//		- Chuan hoa ten
void Student::standardizeN(){
	standardizeName(this->name); 
	cout << "Name is standardized: " << name << endl;
}		
//		- Tach tu, trar ve danh sach tu
void Student::getWords(){
    int *index = new int[count(name)*2];
    index = getIndexOfWords(name);
    
    int t = 0;
	cout << "Word of name: ";
    for ( int i = 0; i < count(name); i++ ) {
    	cout << i + 1 << ". " << getWord(name, index[t], index[t+1]);
    	cout << "   ";
    	t+= 2;
    }	
    cout << endl;
}

//		- So sach xau
void Student::compareName(Student std){
	string str1 = (string) name;
	string str2 = (string) std.name;
	if ( str1.compare(str2) ){
		cout << name << " is greater than " << std.name << endl;
	} else {
		cout << name << " is not greater than" << std.name << endl;
	}
}
//		- Ghep xau
bool Student::addStr(char* str){
	char* newName = new char[strlen(name) + strlen(str)];
	for( int i = 0; i < strlen(name); i++)
		newName[i] = name [i];
	for( int i = 0; i < strlen(str); i++)
		newName[strlen(name) + i] = str[i];
	name = newName;
	cout << "Name is added: " << name << endl;
}

//		- Dao nguoc xau
void Student::inverse(){
    int *index = new int[count(name)*2];
    index = getIndexOfWords(name);
    
    int t = count(name)*2 - 1;
	cout << "Inverse: Word of string: ";
    for ( int i = 0; i < count(name); i++ ) {
    	cout << i + 1 << ". " << getWord(name, index[t -1], index[t]);
    	cout << "   ";
    	t-= 2;
    }
}

int main () {
	Student std1;
	Student std2;
	//- Nhap du lieu vao
	std1.input();
	//- Xuat du lieu ra man hinh
	std1.print();
	//- Chuan hoa ten
	std1.standardizeN();
	//- Tach tu, trar ve danh sach tu
	std1.getWords();
	//- Ghep xau
	std1.addStr(" Lan");
	//- Gan gia tri moi
	std2.setName("Nguyen Thi Thuy");	
	//- So sach xau
	std1.compareName(std2);
	//- Dao nguoc xau
	std1.inverse();

	return 0;
}
