/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex4 of lab4
 * Task		   : a,Xay dung mot lop sinh vien voi cac tinh nang co ban:
 				  Du lieu: ten, nam sinh, diem trung binh
 				b, Xay dung lop danh sach lien ket, cac phan tu trong danh sach la SinhVien
 				o cau a
 				- Nhap
 				- Hien thi
 				- Them, bot sinh vien
 				- Tim kiem theo ten
 				- Loc sinh vien theo khoang diem
 				- Sap xep sinh vien theo ten, theo diem
 * Reference   :
 * Date        : 30/4/2015
 */
#include <iostream>

using namespace std;

class Student{
	private:
	    string name;
	    int birthYear;
	    float score;
	public:
	    Student(){
	        name = "";
	        birthYear = 0;
	        score = 0;
	    }
	    
	    Student( string _name, int _birthYear, int _score ){
	        name = _name;
	        birthYear = _birthYear;
	        score = _score;
	    }
	    string getName()const{
	        return name;
	    }
	    int getBirthYear()const{
	        return birthYear;
	    }
	    float getScore()const{
	        return score;
	    }
	    Student* operator =(const Student &std){
	        name = std.getName();
	        birthYear = std.getBirthYear();
	        score = std.getScore();
	        return this;
	    }
	    bool operator ==(const Student &std){
	        if ( name == std.getName() && score == std.getScore() && birthYear == std.getBirthYear() ) {
	            return true;
	        }else{
	            return false;
	        }
	    }
	    void print(){
	        cout << name << " - " << birthYear << " - " << score << endl;
	    }
};

struct Node{
    Student std;
    Node* next;
};

Node *head=NULL;
Node *tail=NULL;

Node* createNode(string _name, int _birthYear, float _score){
    Node *p = new Node;
    Student data(_name, _birthYear, _score);
    p->std = data;
    p->next = NULL;
    return p;
}
void input(string &name, int &birthYear, float &score){
    cout << "Name: ";
    getline(cin, name);
    cout << "Birthyear: ";
    cin >> birthYear;
    cout << "Score: ";
    cin >> score;
}

void addNode(){
    string name;
    int birthYear;
    float score;
    input(name, birthYear, score);
    Node *p = createNode(name, birthYear, score);
    if ( head == NULL && tail == NULL) {
        head = p;
        tail = p;
    }else{
        tail->next = p;
        tail = p;
    }
    cin.ignore(1);
}
void print(){
    Node* p = head;
    while ( p != NULL) {
        p->std.print();
        p = p->next;
    }
}

void del(){
    //Nhap sv can xoa
    Node *p=head;
    string name;
    int birthYear;
    float score;
    input(name, birthYear, score);
    Student std(name, birthYear, score);
 
    if( head->std == std ){
        head=head->next;
        return;
    }
    while( p->next != NULL){
        if( p->next->std == std ){
            p->next = p->next->next;
            if( p->next == NULL){
                tail = p;
            }
            return;
        }
        p = p->next;
    }
    cout<<"Not found\n";
}
void search(){
    bool check = true;
    string name;
    Node *p=head;
    cout<<"Enter the student name that u wish to find: ";
    getline(cin,name);
    while (p != NULL) {
        if(p->std.getName() == name){
            check = false;
            cout << p->std.getName() << " - " << p->std.getBirthYear() << " - " << p->std.getScore() << endl;
        }
        p = p->next;
    }
    if( check ){
        cout << "Not found\n";
    }
}
void findByScore(){
    float a,b;
    bool check = true;
    Node *p = head;
    cout<"Enter a range of score u wish to find: \n";
    cin >> a >> b;
    while ( p != NULL ) {
        if( p->std.getScore() >= a && p->std.getScore() <= b){
            check = false;
            cout << p->std.getName() << "-" << p->std.getBirthYear() << "-" << p->std.getScore() << endl;
        }
        p = p->next;
    }
    if( check ){
        cout << "Not found\n";
    }
}

void swap(Node *a, Node *b){
    Node c;
    c.std = a->std;
    a->std = b->std;
    b->std = c.std;
}
void sort(){
    bool check = true;
    while( check ){
        check = false;
        Node *p = head;
        while( p->next != NULL ){
            if( p->std.getName().compare(p->next->std.getName()) > 0 ){
                swap(p, p->next);
                check = true;
            }
            p = p->next;
        }
    }
}

int main(){
    int checked=1;
    cout<<"0: Exit\n1: Add\n2: Print\n3: Delete\n4: Search\n5: FindByScore\n6: Sort\n";
    while (checked) {
        cout<<"Enter one of those number below: ";
        cin>>checked;
        cin.ignore(1);
        switch (checked) {
            case 1:
                addNode();
                break;
            case 2:
                print();
                break;
            case 3:
                del();
                break;
            case 4:
                search();
                break;
            case 5:
                findByScore();
                break;
            case 6:
                sort();
                break;
            default:
                break;
        }
    }
    return 0;
}
